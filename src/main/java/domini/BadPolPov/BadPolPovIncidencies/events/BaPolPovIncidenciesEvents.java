package domini.BadPolPov.BadPolPovIncidencies.events;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
 
import org.apache.ofbiz.base.util.Debug;
import org.apache.ofbiz.base.util.UtilMisc;
import org.apache.ofbiz.base.util.UtilValidate;
import org.apache.ofbiz.entity.Delegator;
import org.apache.ofbiz.entity.GenericValue;
import org.apache.ofbiz.service.GenericServiceException;
import org.apache.ofbiz.service.LocalDispatcher;

public class BaPolPovIncidenciesEvents {
	public static final String module = BaPolPovIncidenciesEvents.class.getName();
	
	public static String getBuscarIncidenciesReportParameters(HttpServletRequest request, HttpServletResponse response) {
		String RP_Email=request.getParameter("emailPersona");
		String RP_Name="BadMar"+RP_Email+".pdf";
		
		if(UtilValidate.isEmpty(RP_Email)) { RP_Email="";}
		Map<String, Object>birtParameters=UtilMisc.toMap("RP_email", RP_Email, "RP_Name",RP_Name);
		
		request.setAttribute("birtParameters", birtParameters);
		request.setAttribute("birtOutputFileName", RP_Name);
		return "success";
	}
	public static String getBuscarOrdinadorsReportParameters(HttpServletRequest request, HttpServletResponse response) {
		String RP__ID_Aula=request.getParameter("idAula");
		String RP_Name="PolMar"+RP__ID_Aula+".pdf";
		
		if(UtilValidate.isEmpty(RP__ID_Aula)) { RP__ID_Aula="";}
		Map<String, Object>birtParameters=UtilMisc.toMap("RP_email", RP__ID_Aula, "RP_Name",RP_Name);
		
		request.setAttribute("birtParameters", birtParameters);
		request.setAttribute("birtOutputFileName", RP_Name);
		return "success";
	}
	public static String getBuscarAulaReportParameters(HttpServletRequest request, HttpServletResponse response) {
		String RP_pis=request.getParameter("pis");
		String RP_Name="PovFra"+RP_pis+".pdf";
		
		if(UtilValidate.isEmpty(RP_pis)) { RP_pis="";}
		Map<String, Object>birtParameters=UtilMisc.toMap("RP_pis", Integer.parseInt(RP_pis), "RP_Name",RP_Name);
		
		request.setAttribute("birtParameters", birtParameters);
		request.setAttribute("birtOutputFileName", RP_pis);
		return "success";
	}
	
	public static String createBadPolPovIncidenciaEvent(HttpServletRequest request, HttpServletResponse response) {
        Delegator delegator = (Delegator) request.getAttribute("delegator");
        LocalDispatcher dispatcher = (LocalDispatcher) request.getAttribute("dispatcher");
        GenericValue userLogin = (GenericValue) request.getSession().getAttribute("userLogin");
        
        String idIncidencia = request.getParameter("idIncidencia");
        String gravetat = request.getParameter("gravetat");
        String tipusPersona = request.getParameter("tipusPersona");
        String emailPersona = request.getParameter("emailPersona");
        String dataIncidencia = request.getParameter("dataIncidencia");
 
        if (UtilValidate.isEmpty(gravetat) || 
        	UtilValidate.isEmpty(tipusPersona) || 
        	UtilValidate.isEmpty(emailPersona) || 
        	UtilValidate.isEmpty(dataIncidencia)
        	) {
        	
            String errMsg = "Gravetat, tipus de persona, correu electronic o data de la incidencia no poden estar buits o son incorrectes.";
            request.setAttribute("_ERROR_MESSAGE_", errMsg);
            return "error";
        }
        
        String observacio = request.getParameter("observacio");
        String idAula = request.getParameter("idAula");
        String idOrdinador = request.getParameter("idOrdinador");
        
        try {
            Debug.logInfo("=======Creating Incidencia record in event using service createBadPolPovIncidencies=========", module);
            dispatcher.runSync("createBadPolPovIncidencies", UtilMisc.toMap(
            		"idIncidencia", idIncidencia,
                    "gravetat", gravetat, 
                    "tipusPersona", tipusPersona, 
                    "emailPersona", emailPersona,
                    "observacio", observacio,
                    "dataIncidencia", dataIncidencia,
                    "idAula", idAula,
                    "idOrdinador", idOrdinador,
                    "userLogin", userLogin));
        } catch (GenericServiceException e) {
            String errMsg = "No s'ha pogut crear la incidencia: " + e.toString();
            request.setAttribute("_ERROR_MESSAGE_", errMsg);
            return "error";
        }
        request.setAttribute("_EVENT_MESSAGE_", "Incidencia creada correctament.");
        return "success";
    }
	
	public static String createBadPolPovComponentOrdinadorEvent(HttpServletRequest request, HttpServletResponse response) {
        Delegator delegator = (Delegator) request.getAttribute("delegator");
        LocalDispatcher dispatcher = (LocalDispatcher) request.getAttribute("dispatcher");
        GenericValue userLogin = (GenericValue) request.getSession().getAttribute("userLogin");
        
        String nomComponentOrdinador = request.getParameter("nomComponentOrdinador");
        
 
        if (UtilValidate.isEmpty(nomComponentOrdinador)) {
        	
            String errMsg = "El nom del component d'ordinador no pot estar buit";
            request.setAttribute("_ERROR_MESSAGE_", errMsg);
            return "error";
        }
        
        String idOrdinador = request.getParameter("idOrdinador");
        
        try {
            Debug.logInfo("=======Creating Incidencia record in event using service createBadPolPovIncidencies=========", module);
            dispatcher.runSync("createBadPolPovCompOrdinador", UtilMisc.toMap(
            		"nomComponentOrdinador", nomComponentOrdinador,
                    "idOrdinador", idOrdinador,
                    "userLogin", userLogin));
        } catch (GenericServiceException e) {
            String errMsg = "No s'ha pogut afegir el component: " + e.toString();
            request.setAttribute("_ERROR_MESSAGE_", errMsg);
            return "error";
        }
        request.setAttribute("_EVENT_MESSAGE_", "Component afegit correctament.");
        return "success";
    }
	
	
	
}
